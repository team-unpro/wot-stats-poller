package com.teamunpro.exception;

import com.teamunpro.wot.dto.WotError;

public class WotApiException extends Exception {
    public WotApiException(WotError error) {
        super(error.getMessage());
    }
}
