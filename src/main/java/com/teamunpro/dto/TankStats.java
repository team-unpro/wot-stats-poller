package com.teamunpro.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.teamunpro.wot.dto.ExpectedTankValues;
import lombok.Data;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class TankStats implements HasId {
    @JsonProperty("player_id")
    private int playerId;
    @JsonProperty("tank_id")
    private int tankId;
    private int frags;
    private int damage;
    private int spot;
    private int wins;
    private int defense;
    private int battles;
    @JsonProperty("@timestamp")
    private ZonedDateTime timestamp = ZonedDateTime.now(ZoneId.of("UTC"));
    @JsonIgnore
    private ExpectedTankValues expectedStats;

    @JsonIgnore
    public double getWN8() {
        double rDAMAGE = (getDamage() / (double) getBattles()) / expectedStats.getExpectedDamage();
        double rSPOT = (getSpot() / (double) getBattles()) / expectedStats.getExpectedSpot();
        double rFRAG = (getFrags() / (double) getBattles()) / expectedStats.getExpectedFrags();
        double rDEF = (getDefense() / (double) getBattles()) / expectedStats.getExpectedDefense();
        double rWIN = (getWins() / (double) getBattles()) * 100 / expectedStats.getExpectedWinRate();

        double rWINc = Math.max(0, (rWIN - 0.71) / (1 - 0.71));
        double rDAMAGEc = Math.max(0, (rDAMAGE - 0.22) / (1 - 0.22));
        double rFRAGc = Math.max(0, Math.min(rDAMAGEc + 0.2, (rFRAG   - 0.12) / (1 - 0.12)));
        double rSPOTc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rSPOT   - 0.38) / (1 - 0.38)));
        double rDEFc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rDEF    - 0.10) / (1 - 0.10)));
        return 980 * rDAMAGEc +
                210 * rDAMAGEc * rFRAGc +
                155 * rFRAGc * rSPOTc +
                75 * rDEFc * rFRAGc +
                145 * Math.min(1.8, rWINc);
    }

    @JsonIgnore
    @Override
    public String getId() {
        return String.join("", Integer.toString(playerId), Integer.toString(tankId), getTimestamp().toLocalDateTime().format(DateTimeFormatter.ISO_DATE_TIME));
    }

    @JsonIgnore
    public boolean isIdentity() {
        return frags == 0 && damage == 0 && spot == 0 && defense == 0 && wins == 0 && battles == 0;
    }
}
