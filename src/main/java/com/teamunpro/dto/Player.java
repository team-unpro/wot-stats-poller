package com.teamunpro.dto;

import com.teamunpro.domain.Clan;
import lombok.Data;

@Data
public class Player {
    private int playerId;
    private String name;
    private Clan clan;
}
