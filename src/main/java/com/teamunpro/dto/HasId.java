package com.teamunpro.dto;

public interface HasId {
    String getId();
}
