package com.teamunpro.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Entity
@Data
@Table(name = "clan_membership",
        uniqueConstraints=@UniqueConstraint(columnNames = {"player_id", "date"}))
public class ClanMembership {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "clan_id")
    private Clan clan;
    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;
    @Column
    private LocalDate date = LocalDate.now(ZoneId.of("UTC"));
}
