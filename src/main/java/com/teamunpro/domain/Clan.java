package com.teamunpro.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "clan")
public class Clan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "clan_id", unique = true)
    private int clanId;
    @Column
    private String name;
}
