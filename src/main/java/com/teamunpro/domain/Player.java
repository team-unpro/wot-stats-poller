package com.teamunpro.domain;

import lombok.Data;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "player")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "player_id", unique = true)
    private int playerId;
    @Column
    private String name;
    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "player_id")
    @Cascade({CascadeType.PERSIST, CascadeType.SAVE_UPDATE})
    private List<ClanMembership> clanMemberships;
}
