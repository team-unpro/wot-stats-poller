package com.teamunpro;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamunpro.domain.Clan;
import com.teamunpro.domain.Player;
import com.teamunpro.dto.TankStats;
import com.teamunpro.processor.*;
import com.teamunpro.reader.RemoteFrontInfoReader;
import com.teamunpro.reader.RemoteClanIdReader;
import com.teamunpro.reader.RemotePlayerIdReader;
import com.teamunpro.reader.RemoteTankStatsReader;
import com.teamunpro.repository.BaseRepositoryImpl;
import com.teamunpro.repository.ClanRepository;
import com.teamunpro.repository.PlayerRepository;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.FrontInfo;
import com.teamunpro.writer.ElasticsearchTankStatsWriter;
import org.elasticsearch.client.transport.TransportClient;
import org.hibernate.SessionFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.HibernateItemWriter;
import org.springframework.batch.item.database.HibernatePagingItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableBatchProcessing
@EnableJpaRepositories(repositoryBaseClass = BaseRepositoryImpl.class)
public class WotStatsPoller {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private TransportClient client;

    @Autowired
    private WotApiClient wotApiClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private FrontInfoProcessor frontInfoProcessor;

    @Autowired
    private ClanItemProcessor clanItemProcessor;

    @Autowired
    private PlayerMergerItemProcessor playerMergerItemProcessor;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    @Qualifier("tankStatsReaderExecutor")
    private SimpleAsyncTaskExecutor tankStatsReaderExecutor;

    @Autowired
    @Qualifier("elasticsearchWriterExecutor")
    private TaskExecutor elasticsearchWriterExecutor;

    @Value("${elasticsearch.index}")
    private String index;

    @Autowired
    private ClanRepository clanRepository;

    @Autowired
    private PlayerRepository playerRepository;

    // tag::readerwriterprocessor[]
    @Bean
    public RemoteFrontInfoReader remoteFrontInfoReader() {
        return new RemoteFrontInfoReader(wotApiClient);
    }

    @Bean
    public FlatFileItemWriter<FrontInfo> frontInfoItemWriter() {
        FlatFileItemWriter<FrontInfo> frontInfoFlatFileItemWriter = new FlatFileItemWriter<>();
        frontInfoFlatFileItemWriter.setResource(new FileSystemResource("data/fronts.csv"));
        DelimitedLineAggregator<FrontInfo> lineAggregator = new DelimitedLineAggregator<>();
        BeanWrapperFieldExtractor<FrontInfo> fieldExtractor = new BeanWrapperFieldExtractor<>();
        fieldExtractor.setNames(new String[]{"frontId"});
        lineAggregator.setFieldExtractor(fieldExtractor);
        frontInfoFlatFileItemWriter.setLineAggregator(lineAggregator);
        return frontInfoFlatFileItemWriter;
    }

    @Bean
    public ItemReader<Integer> remoteClanIdReader() {
        RemoteClanIdReader remoteClanIdReader = new RemoteClanIdReader();
        remoteClanIdReader.setClient(wotApiClient);
        FlatFileItemReader<FrontInfo> frontInfoReader = new FlatFileItemReader<>();
        frontInfoReader.setResource(new FileSystemResource("data/fronts.csv"));
        DefaultLineMapper<FrontInfo> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(new String[]{"frontId"});
        lineMapper.setLineTokenizer(tokenizer);
        BeanWrapperFieldSetMapper<FrontInfo> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(FrontInfo.class);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        frontInfoReader.setLineMapper(lineMapper);
        remoteClanIdReader.setFrontInfoReader(frontInfoReader);
        return remoteClanIdReader;
    }

    @Bean
    public HibernatePagingItemReader<Clan> clanReader() {
        HibernatePagingItemReader<Clan> itemReader = new HibernatePagingItemReader<>();
        itemReader.setSessionFactory(sessionFactory);
        itemReader.setQueryString("from Clan");
        return itemReader;
    }

    @Bean
    public HibernatePagingItemReader<Player> playerReader() {
        HibernatePagingItemReader<Player> itemReader = new HibernatePagingItemReader<>();
        itemReader.setSessionFactory(sessionFactory);
        itemReader.setQueryString("from Player");
        return itemReader;
    }

    @Bean
    public ItemWriter<Clan> clanWriter() {
        HibernateItemWriter<Clan> writer = new HibernateItemWriter<>();
        writer.setSessionFactory(sessionFactory);
        return writer;
    }

    @Bean
    public ItemReader<Integer> remotePlayerIdReader() {
        RemotePlayerIdReader remotePlayerIdReader = new RemotePlayerIdReader();
        remotePlayerIdReader.setClient(wotApiClient);
        remotePlayerIdReader.setClanRepository(clanRepository);
        return remotePlayerIdReader;
    }

    @Bean
    public ItemWriter<Player> playerWriter() {
        HibernateItemWriter<Player> writer = new HibernateItemWriter<>();
        writer.setSessionFactory(sessionFactory);
        return writer;
    }

    @Bean
    public ItemWriter<TankStats> tankStatsWriter() {
        return new ElasticsearchTankStatsWriter(client, objectMapper, index, "tank_stats");
    }
    // end::readerwriterprocessor[]

    // tag::jobstep[]
    @Bean
    public Step importFrontsStep() {
        return stepBuilderFactory.get("importFrontsStep")
                .<FrontInfo, FrontInfo>chunk(10)
                .reader(remoteFrontInfoReader())
                .writer(frontInfoItemWriter())
                .build();
    }

    @Bean
    public Step importClansStep() {
        CompositeItemProcessor<Integer, Clan> processor = new CompositeItemProcessor<>();
        processor.setDelegates(Arrays.asList(frontInfoProcessor, new CollectionToListProcessor<Clan>()));
        return stepBuilderFactory.get("importClansStep")
                .<Integer, Clan> chunk(10)
                .reader(remoteClanIdReader())
                .processor(item -> {
                    Clan clan = new Clan();
                    clan.setClanId(item);
                    return clan;
                })
                .writer(clanWriter())
                .build();
    }

    private Step importPlayersStep() {
        CompositeItemProcessor<Clan, List<Player>> processor = new CompositeItemProcessor<>();
        processor.setDelegates(Arrays.asList(clanItemProcessor, playerMergerItemProcessor));
        return stepBuilderFactory.get("importPlayersStep")
                .<Integer, Player> chunk(10)
                .reader(remotePlayerIdReader())
                .processor(item -> {
                    Player player = new Player();
                    player.setPlayerId(item);
                    return player;
                })
                .writer(playerWriter())
                .build();
    }

    @Bean
    public Step playersToTankStatsStep() {
        return stepBuilderFactory.get("playersToTankStatsStep")
                .<TankStats, TankStats> chunk(1000)
                .reader(tankStatsReader())
                .writer(tankStatsWriter())
                .taskExecutor(elasticsearchWriterExecutor)
                .build();
    }

    private ItemReader<TankStats> tankStatsReader() {
        RemoteTankStatsReader remoteTankStatsReader = new RemoteTankStatsReader();
        remoteTankStatsReader.setClient(wotApiClient);
        remoteTankStatsReader.setPlayerRepository(playerRepository);
        remoteTankStatsReader.setExecutor(tankStatsReaderExecutor);
        return remoteTankStatsReader;
    }

    @Bean
    public Job importClans() {
        return jobBuilderFactory.get("importClans")
                .incrementer(new RunIdIncrementer())
                .flow(importFrontsStep())
                .next(importClansStep())
                .end()
                .build();
    }

    @Bean
    public Job importPlayers() {
        return jobBuilderFactory.get("importPlayers")
                .incrementer(new RunIdIncrementer())
                .flow(importPlayersStep())
                .end()
                .build();
    }

    @Bean
    public Job importPlayerStats() {
        return jobBuilderFactory.get("importTankStats")
                .incrementer(new RunIdIncrementer())
                .flow(playersToTankStatsStep())
                .end()
                .build();
    }
    // end::jobstep[]

    public static void main(String[] args) {
        System.exit(SpringApplication
                .exit(SpringApplication.run(WotStatsPoller.class, args)));
    }
}
