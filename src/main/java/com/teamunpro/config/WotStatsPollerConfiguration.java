package com.teamunpro.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.teamunpro.repository.BaseRepositoryImpl;
import com.teamunpro.wot.dto.ExpectedTankValuesContainer;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.sql.DataSource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class WotStatsPollerConfiguration {
    @Value("#{'${elasticsearch.hosts}'.split(',')}")
    private List<String> hosts;

    @Bean
    public TransportClient elasticsearchClient() {
        return new PreBuiltTransportClient(Settings.EMPTY)
                .addTransportAddresses((TransportAddress[]) hosts.stream()
                        .map(h -> {
                            try {
                                return new InetSocketTransportAddress(InetAddress.getByName(h), 9300);
                            } catch (UnknownHostException e) {
                                throw new RuntimeException(e);
                            }
                        })
                        .collect(Collectors.toList()).toArray(new TransportAddress[hosts.size()]));
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    @Bean
    public ExpectedTankValuesContainer expectedTankValuesContainer() throws IOException {
        return objectMapper().reader().forType(ExpectedTankValuesContainer.class).readValue(new ClassPathResource("expected_tank_values_28.json").getInputStream());
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(dataSource);
        localSessionFactoryBean.setPackagesToScan("com.teamunpro.domain");
        return localSessionFactoryBean;
    }

    @Bean
    public Client jerseyClient() {
        ClientConfig config = new ClientConfig();
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();;
        ApacheConnectorProvider connectorProvider = new ApacheConnectorProvider();
        connectionManager.setMaxTotal(100);
        connectionManager.setDefaultMaxPerRoute(20);
        config.property(ApacheClientProperties.CONNECTION_MANAGER, connectionManager);
        config.connectorProvider(connectorProvider);

        return ClientBuilder.newClient(config);
    }

    @Bean
    public SimpleAsyncTaskExecutor tankStatsReaderExecutor() {
        SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
        simpleAsyncTaskExecutor.setConcurrencyLimit(30);
        return simpleAsyncTaskExecutor;
    }

    @Bean
    public SimpleAsyncTaskExecutor elasticsearchWriterExecutor() {
        return new SimpleAsyncTaskExecutor();
    }
}
