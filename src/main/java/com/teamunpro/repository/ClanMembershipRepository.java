package com.teamunpro.repository;

import com.teamunpro.domain.ClanMembership;
import com.teamunpro.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClanMembershipRepository extends BaseRepository<ClanMembership, Integer> {
    Optional<ClanMembership> findFirstByPlayerOrderByDateDesc(Player player);
}
