package com.teamunpro.repository;

import com.teamunpro.domain.Clan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClanRepository extends BaseRepository<Clan, Integer> {
}
