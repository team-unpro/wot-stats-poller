package com.teamunpro.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;

public class BaseRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements BaseRepository<T, ID> {

    private final EntityManager entityManager;

    public BaseRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    public List<T> findAll(int offset, int limit) {
        return findAll(offset, limit, null);
    }

    public List<T> findAll(int offset, int limit, Sort sort) {
        TypedQuery<T> query = getQuery(null, sort);

        if (offset < 0) {
            throw new IllegalArgumentException("Offset must not be less than zero!");
        }
        if (limit < 1) {
            throw new IllegalArgumentException("Max results must not be less than one!");
        }

        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

}
