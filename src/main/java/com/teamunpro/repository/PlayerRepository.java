package com.teamunpro.repository;

import com.teamunpro.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlayerRepository extends BaseRepository<Player, Integer>, JpaSpecificationExecutor {
    Optional<Player> findFirstByPlayerId(Integer playerId);
}
