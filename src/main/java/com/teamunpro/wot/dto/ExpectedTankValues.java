package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ExpectedTankValues {
    @JsonProperty(value = "IDNum")
    private int tankId;
    @JsonProperty(value = "expFrag")
    private double expectedFrags;
    @JsonProperty(value = "expDamage")
    private double expectedDamage;
    @JsonProperty(value = "expSpot")
    private double expectedSpot;
    @JsonProperty(value = "expDef")
    private double expectedDefense;
    @JsonProperty(value = "expWinRate")
    private double expectedWinRate;
}
