package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TankStatsContainer {
    private TankStats random;
    @JsonProperty(value = "tank_id")
    private int tankId;
}
