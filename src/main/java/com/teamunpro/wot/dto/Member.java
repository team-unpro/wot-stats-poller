package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Member {
    @JsonProperty("account_id")
    private int accountId;
    @JsonProperty("account_name")
    private String accountName;
}
