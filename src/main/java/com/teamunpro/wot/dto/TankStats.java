package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TankStats {
    private int spotted;
    private int wins;
    private int battles;
    @JsonProperty(value = "damage_dealt")
    private int damageDealt;
    private int frags;
    @JsonProperty(value = "dropped_capture_points")
    private int droppedCapturePoints;
}
