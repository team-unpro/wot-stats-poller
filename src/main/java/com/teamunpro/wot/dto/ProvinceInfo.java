package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProvinceInfo {
    @JsonProperty(value = "owner_clan_id")
    private int ownerClanId;
    private List<Integer> competitors;
}
