package com.teamunpro.wot.dto;

import lombok.Data;

import java.util.List;

@Data
public class ClanDetails {
    private List<Member> members;
}
