package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProvinceInfoContainer extends WotListResponse<ProvinceInfo> {
}
