package com.teamunpro.wot.dto;

import lombok.Data;

@Data
public class WotResponse {
    private WotError error;
    private String status;
}
