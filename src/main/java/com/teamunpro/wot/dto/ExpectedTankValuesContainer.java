package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExpectedTankValuesContainer {
    private List<ExpectedTankValues> data;
}
