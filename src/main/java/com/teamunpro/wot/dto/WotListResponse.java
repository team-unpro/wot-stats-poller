package com.teamunpro.wot.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper=true)
class WotListResponse<T> extends WotResponse {
    private List<T> data;
}
