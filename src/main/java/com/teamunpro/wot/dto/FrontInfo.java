package com.teamunpro.wot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FrontInfo {
    @JsonProperty(value = "front_id")
    String frontId;
}
