package com.teamunpro.wot.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@EqualsAndHashCode(callSuper=true)
class WotMapResponse<T> extends WotResponse {
    private Map<String, T> data;
}
