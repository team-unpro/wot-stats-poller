package com.teamunpro.wot;

import com.google.common.util.concurrent.RateLimiter;
import com.teamunpro.exception.WotApiException;
import com.teamunpro.wot.dto.WotResponse;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.client.WebTarget;

@Slf4j
public class WotApiRequestBuilder {
    private final WebTarget webTarget;
    private final RateLimiter rateLimiter;

    public WotApiRequestBuilder(WebTarget webTarget, RateLimiter rateLimiter) {
        this.webTarget = webTarget;
        this.rateLimiter = rateLimiter;
    }

    public WotApiRequestBuilder queryParam(String name, Object value) {
        return new WotApiRequestBuilder(webTarget.queryParam(name, value), this.rateLimiter);
    }

    public <T extends WotResponse> T get(Class<T> responseType) throws WotApiException {
        double waitTime = this.rateLimiter.acquire();
        if (waitTime != 0) {
            log.debug("Waited " + waitTime + " for lock.");
        }
        T resp = webTarget.request().get(responseType);
        if (resp.getStatus().equals("error")) {
            throw new WotApiException(resp.getError());
        }
        return resp;
    }
}
