package com.teamunpro.writer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamunpro.dto.TankStats;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.batch.item.ItemWriter;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class ElasticsearchTankStatsWriter implements ItemWriter<TankStats> {
    private TransportClient client;
    private ObjectMapper objectMapper;
    private String index;
    private String type;

    public ElasticsearchTankStatsWriter(TransportClient client, ObjectMapper objectMapper, String index, String type) {
        this.client = client;
        this.objectMapper = objectMapper;
        this.index = index;
        this.type = type;
    }

    @Override
    public void write(List<? extends TankStats> items) throws Exception {
        MultiSearchRequestBuilder msrb = client.prepareMultiSearch();
        items.stream().map(this::createSearchRequest).forEach(msrb::add);
        MultiSearchResponse msr = msrb.get();
        BulkProcessor b = BulkProcessor.builder(client, new BulkProcessorListener())
                .build();
        IntStream.range(0, items.size())
                .boxed()
                .map(i -> createIndexRequest(msr.getResponses()[i], items.get(i)))
                .filter(Objects::nonNull)
                .forEach(b::add);
        b.awaitClose(5, TimeUnit.MINUTES);
    }

    private IndexRequest createIndexRequest(MultiSearchResponse.Item item, TankStats tankStats) {
        TankStats diff;
        if (!item.isFailure() && item.getResponse().getHits().getTotalHits() > 0) {
            diff = new TankStats();
            try {
                TankStats previous = objectMapper.readerFor(TankStats.class).readValue(item.getResponse().getHits().getHits()[0].source());
                diff.setPlayerId(tankStats.getPlayerId());
                diff.setTankId(tankStats.getTankId());
                diff.setBattles(tankStats.getBattles() - previous.getBattles());
                diff.setDamage(tankStats.getDamage() - previous.getDamage());
                diff.setDamage(tankStats.getFrags() - previous.getFrags());
                diff.setDamage(tankStats.getDefense() - previous.getDefense());
                diff.setDamage(tankStats.getSpot() - previous.getSpot());
                diff.setDamage(tankStats.getWins() - previous.getWins());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            diff = tankStats;
        }
        if (!diff.isIdentity()) {
            try {
                return client.prepareIndex(index, type, diff.getId()).setSource(objectMapper.writeValueAsBytes(diff)).request();
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        } else {
            return null;
        }
    }

    private SearchRequestBuilder createSearchRequest(TankStats tankStats) {
        return client.prepareSearch(index)
                .setTypes(type)
                .setQuery(
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.termQuery("player_id", tankStats.getPlayerId()))
                                .must(QueryBuilders.termQuery("tank_id", tankStats.getTankId()))
                                .must(QueryBuilders.rangeQuery("@timestamp").lt(ZonedDateTime.now(ZoneId.of("UTC")).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)))
                                .mustNot(QueryBuilders.idsQuery(type).addIds(tankStats.getId())))
                .addSort("@timestamp", SortOrder.DESC)
                .setSize(1);
    }

    private static class BulkProcessorListener implements BulkProcessor.Listener {
        @Override
        public void beforeBulk(long executionId, BulkRequest request) {

        }

        @Override
        public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {

        }

        @Override
        public void afterBulk(long executionId, BulkRequest request, Throwable failure) {

        }
    }
}
