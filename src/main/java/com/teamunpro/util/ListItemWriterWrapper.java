package com.teamunpro.util;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class ListItemWriterWrapper<T> implements ItemWriter<List<T>> {
    private ItemWriter<T> wrapped;

    public ListItemWriterWrapper(ItemWriter<T> wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public void write(List<? extends List<T>> items) throws Exception {
        for (List<T> l : items) {
            if (!l.isEmpty()) {
                wrapped.write(l);
            }
        }
    }
}
