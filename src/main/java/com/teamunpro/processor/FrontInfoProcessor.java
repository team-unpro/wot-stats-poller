package com.teamunpro.processor;

import com.teamunpro.domain.Clan;
import com.teamunpro.exception.WotApiException;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.FrontInfo;
import com.teamunpro.wot.dto.ProvinceInfoContainer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FrontInfoProcessor implements ItemProcessor<FrontInfo, Set<Clan>> {
    private static final String[] fields = new String[] {
            "owner_clan_id", "competitors"
    };

    private final WotApiClient client;

    @Autowired
    public FrontInfoProcessor(WotApiClient client) {
        this.client = client;
    }

    public Set<Clan> process(FrontInfo front) throws WotApiException {
        ProvinceInfoContainer response = client.target("wot/globalmap/provinces/")
                .queryParam("fields", String.join(",", (CharSequence[]) fields))
                .queryParam("front_id", front.getFrontId())
                .get(ProvinceInfoContainer.class);

        return response.getData().stream()
                .flatMap(p -> Stream.concat(Stream.of(p.getOwnerClanId()), p.getCompetitors().stream()))
                .map(c -> {
                    Clan clan = new Clan();
                    clan.setClanId(c);
                    return clan;
                }).collect(Collectors.toSet());
    }
}
