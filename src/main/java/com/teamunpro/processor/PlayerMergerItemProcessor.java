package com.teamunpro.processor;

import com.teamunpro.domain.ClanMembership;
import com.teamunpro.domain.Player;
import com.teamunpro.repository.ClanMembershipRepository;
import com.teamunpro.repository.PlayerRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlayerMergerItemProcessor implements ItemProcessor<List<com.teamunpro.dto.Player>, List<Player>> {
    @Autowired
    private ClanMembershipRepository clanMembershipRepository;
    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public List<Player> process(List<com.teamunpro.dto.Player> item) {
        return item.stream().map(this::processOne).collect(Collectors.toList());
    }

    private Player processOne(com.teamunpro.dto.Player item) {
        Optional<Player> op = playerRepository.findFirstByPlayerId(item.getPlayerId());
        Player p;
        if (op.isPresent()) {
            p = op.get();
            Optional<ClanMembership> ocm = clanMembershipRepository.findFirstByPlayerOrderByDateDesc(p);
            if (!ocm.isPresent() || !ocm.get().getClan().equals(item.getClan())) {
                ClanMembership cm = new ClanMembership();
                cm.setClan(item.getClan());
                cm.setPlayer(op.get());
                op.get().getClanMemberships().add(cm);
            }
        } else {
            p = new Player();
            ClanMembership cm = new ClanMembership();
            cm.setClan(item.getClan());
            cm.setPlayer(p);
            p.setPlayerId(item.getPlayerId());
            p.setName(item.getName());
            p.setClanMemberships(Collections.singletonList(cm));
        }

        return p;
    }
}
