package com.teamunpro.processor;

import com.teamunpro.domain.Player;
import com.teamunpro.dto.TankStats;
import com.teamunpro.exception.WotApiException;
import com.teamunpro.service.ExpectedTankValuesService;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.PlayerStatsContainer;
import com.teamunpro.wot.dto.TankStatsContainer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlayerItemProcessor implements ItemProcessor<Player, List<TankStats>> {
    private static final String[] fields = new String[] {
            "tank_id", "random.frags", "random.damage_dealt", "random.battles", "random.wins",
            "random.spotted", "random.dropped_capture_points"
    };

    private final ExpectedTankValuesService expectedTankValuesService;
    private final WotApiClient client;

    @Autowired
    public PlayerItemProcessor(ExpectedTankValuesService expectedTankValuesService, WotApiClient client) {
        this.expectedTankValuesService = expectedTankValuesService;
        this.client = client;
    }

    @Override
    public List<TankStats> process(Player item) throws WotApiException {
        PlayerStatsContainer response = client.target("wot/tanks/stats/")
                .queryParam("account_id", item.getPlayerId())
                .queryParam("extra", "random")
                .queryParam("fields", String.join(",", (CharSequence[]) fields))
                .get(PlayerStatsContainer.class);
        // Need null check because somehow there are some players on clans that have no tank stats
        return Optional.ofNullable(response.getData().get(Integer.valueOf(item.getPlayerId()).toString()))
                .map(l -> l.stream()
                        .filter(tsc -> tsc.getRandom().getBattles() != 0)
                        .filter(tsc -> expectedTankValuesService.getExpectedTankValues(tsc.getTankId()) != null)
                        .map(ts -> convert(item, ts))
                        .collect(Collectors.toList())).orElse(null);
    }

    private TankStats convert(Player item, TankStatsContainer tsc) {
        TankStats tankStats = new TankStats();
        tankStats.setPlayerId(item.getPlayerId());
        tankStats.setTankId(tsc.getTankId());
        tankStats.setDamage(tsc.getRandom().getDamageDealt());
        tankStats.setFrags(tsc.getRandom().getFrags());
        tankStats.setSpot(tsc.getRandom().getSpotted());
        tankStats.setWins(tsc.getRandom().getWins());
        tankStats.setDefense(tsc.getRandom().getDroppedCapturePoints());
        tankStats.setExpectedStats(expectedTankValuesService.getExpectedTankValues(tsc.getTankId()));
        tankStats.setBattles(tsc.getRandom().getBattles());
        return tankStats;
    }
}
