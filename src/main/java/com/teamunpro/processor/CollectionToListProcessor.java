package com.teamunpro.processor;

import org.springframework.batch.item.ItemProcessor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CollectionToListProcessor<T> implements ItemProcessor<Collection<T>, List<T>> {
    @Override
    public List<T> process(Collection<T> item) throws Exception {
        return new ArrayList<>(item);
    }
}
