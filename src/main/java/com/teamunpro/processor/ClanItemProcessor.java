package com.teamunpro.processor;

import com.teamunpro.domain.Clan;
import com.teamunpro.dto.Player;
import com.teamunpro.exception.WotApiException;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.ClanDetailsContainer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClanItemProcessor implements ItemProcessor<Clan, List<Player>> {
    private static final String[] fields = new String[] {
            "members.account_id",
            "members.account_name"
    };

    private final WotApiClient client;

    @Autowired
    public ClanItemProcessor(WotApiClient client) {
        this.client = client;
    }

    @Override
    public List<Player> process(Clan item) throws WotApiException {
        // TODO: this API endpoint can accept multiple clans. Batch these calls together somehow
        ClanDetailsContainer response = client.target("wgn/clans/info/")
                .queryParam("clan_id", item.getClanId())
                .queryParam("fields", String.join(",", (CharSequence[]) fields))
                .get(ClanDetailsContainer.class);
        return response.getData().get(Integer.toString(item.getClanId())).getMembers().stream().map(m -> {
            Player p = new Player();
            p.setPlayerId(m.getAccountId());
            p.setName(m.getAccountName());
            p.setClan(item);
            return p;
        }).collect(Collectors.toList());
    }
}
