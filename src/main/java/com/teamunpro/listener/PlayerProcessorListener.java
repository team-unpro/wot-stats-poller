package com.teamunpro.listener;

import com.teamunpro.domain.Player;
import com.teamunpro.dto.TankStats;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class PlayerProcessorListener implements ItemProcessListener<Player, List<TankStats>> {
    private AtomicInteger counter = new AtomicInteger();

    @Override
    public void beforeProcess(Player item) {

    }

    @Override
    public void afterProcess(Player item, List<TankStats> result) {
        int v = counter.incrementAndGet();
        if (v % 100 == 0) {
            log.info("Processed " + v + " players");
        }
    }

    @Override
    public void onProcessError(Player item, Exception e) {

    }
}
