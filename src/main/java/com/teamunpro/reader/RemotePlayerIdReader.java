package com.teamunpro.reader;

import com.teamunpro.domain.Clan;
import com.teamunpro.exception.WotApiException;
import com.teamunpro.repository.ClanRepository;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.ClanDetailsContainer;
import com.teamunpro.wot.dto.Member;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class RemotePlayerIdReader extends AbstractRemoteBatchingItemReader<Integer> {
    private static final String[] fields = new String[] {
            "members.account_id"
    };

    private WotApiClient client;
    private ClanRepository clanRepository;
    private int page = 0;

    public void setClient(WotApiClient client) {
        this.client = client;
    }

    public void setClanRepository(ClanRepository clanRepository) {
        this.clanRepository = clanRepository;
    }

    @Override
    protected void doReadBatch() {
        if (results == null) {
            results = new CopyOnWriteArrayList<>();
        } else {
            results.clear();
        }
        List<String> ids = clanRepository.findAll(new PageRequest(page++, 100)).getContent().stream()
                .mapToInt(Clan::getClanId)
                .boxed()
                .map(Object::toString)
                .collect(Collectors.toList());

        if (!ids.isEmpty()) {
            ClanDetailsContainer response;
            try {
                response = client.target("wgn/clans/info/")
                        .queryParam("clan_id", String.join(",", ids))
                        .queryParam("fields", String.join(",", (CharSequence[]) fields))
                        .get(ClanDetailsContainer.class);
            } catch (WotApiException e) {
                throw new RuntimeException(e);
            }

            results.addAll(response.getData().values().stream()
                    .flatMap(cd -> cd.getMembers().stream())
                    .map(Member::getAccountId)
                    .collect(Collectors.toList()));
        }
    }
}
