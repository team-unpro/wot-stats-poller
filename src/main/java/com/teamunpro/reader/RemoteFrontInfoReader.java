package com.teamunpro.reader;

import com.teamunpro.exception.WotApiException;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.FrontInfo;
import com.teamunpro.wot.dto.FrontInfoContainer;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

public class RemoteFrontInfoReader implements ItemReader<FrontInfo> {
    private final static String[] fields = new String[] {
            "front_id"
    };

    private final WotApiClient client;
    private FrontInfoContainer currentResponse;
    private int currentPosition;

    @Autowired
    public RemoteFrontInfoReader(WotApiClient client) {
        this.client = client;
    }

    public FrontInfo read() throws WotApiException {
        if (currentResponse == null) {
            currentResponse = client.target("wot/globalmap/fronts/")
                    .queryParam("fields", String.join(",", (CharSequence[]) fields))
                    .get(FrontInfoContainer.class);
        }

        FrontInfo frontInfo;

        if (currentResponse.getData().size() > currentPosition) {
            frontInfo = currentResponse.getData().get(currentPosition);
            currentPosition += 1;
        } else {
            frontInfo = null;
        }

        return frontInfo;
    }
}
