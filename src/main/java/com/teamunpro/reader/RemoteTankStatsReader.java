package com.teamunpro.reader;

import com.teamunpro.domain.Player;
import com.teamunpro.dto.TankStats;
import com.teamunpro.exception.WotApiException;
import com.teamunpro.repository.PlayerRepository;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.PlayerStatsContainer;
import com.teamunpro.wot.dto.TankStatsContainer;
import org.springframework.batch.item.support.AbstractItemStreamItemReader;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RemoteTankStatsReader extends AbstractItemStreamItemReader<TankStats> {
    private static final String[] fields = new String[]{
            "tank_id", "random.frags", "random.damage_dealt", "random.battles", "random.wins",
            "random.spotted", "random.dropped_capture_points"
    };

    private List<TankStats> results;
    private List<CompletableFuture> futures;
    private int current = 0;
    private final Object lock = new Object();
    private WotApiClient client;
    private PlayerRepository playerRepository;
    private int playerOffset = 0;
    private SimpleAsyncTaskExecutor executor;

    @Override
    public TankStats read() throws Exception {
        synchronized (lock) {

            if (results == null || current >= results.size()) {
                doBlockingRead();
            }

            int next = current++;
            if (next < results.size()) {
                return results.get(next);
            } else {
                return null;
            }

        }
    }

    private void doBlockingRead() throws Exception {
        if (results == null) {
            results = new CopyOnWriteArrayList<>();
        }
        if (futures == null) {
            futures = new CopyOnWriteArrayList<>(new CompletableFuture[executor.getConcurrencyLimit()]);
        }

        List<Integer> nulls = new ArrayList<>(executor.getConcurrencyLimit());
        for (int i = 0; i < futures.size(); i++) {
            if (futures.get(i) == null || futures.get(i).isDone()) {
                nulls.add(i);
            }
        }

        List<CompletableFuture<Boolean>> futuresToAdd = playerRepository.findAll(playerOffset, nulls.size()).stream()
                .mapToInt(Player::getPlayerId)
                .boxed()
                .map(Object::toString)
                .map(this::convertToFuture)
                .collect(Collectors.toList());
        for (int i = 0; i < nulls.size(); i++) {
            if (i < futuresToAdd.size()) {
                futures.set(nulls.get(i), futuresToAdd.get(i));
            } else {
                futures.set(nulls.get(i), null);
            }
        }
        playerOffset += futuresToAdd.size();

        CompletableFuture[] cfs = futures.stream()
                .filter(Objects::nonNull)
                .toArray(CompletableFuture[]::new);
        if (cfs.length != 0) {
            CompletableFuture cf = CompletableFuture.anyOf(cfs);
            cf.get();
        }
    }

    private CompletableFuture<Boolean> convertToFuture(String id) {
        return CompletableFuture.supplyAsync(new GetTankStatsSupplier(id), executor).thenApply(tsc -> results.addAll(tsc));
    }

    private Stream<TankStats> getStreamOfTankStats(Map.Entry<String, List<TankStatsContainer>> entry) {
        return Optional.ofNullable(entry.getValue())
                .map(v -> v.stream()
                        .map(tsc -> getTankStats(entry.getKey(), tsc)))
                .orElse(Stream.empty());
    }

    private TankStats getTankStats(String playerId, TankStatsContainer tsc) {
        TankStats tankStats = new TankStats();
        tankStats.setPlayerId(Integer.parseInt(playerId));
        tankStats.setTankId(tsc.getTankId());
        tankStats.setDamage(tsc.getRandom().getDamageDealt());
        tankStats.setFrags(tsc.getRandom().getFrags());
        tankStats.setSpot(tsc.getRandom().getSpotted());
        tankStats.setWins(tsc.getRandom().getWins());
        tankStats.setDefense(tsc.getRandom().getDroppedCapturePoints());
        tankStats.setBattles(tsc.getRandom().getBattles());
        return tankStats;
    }

    public void setClient(WotApiClient client) {
        this.client = client;
    }

    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void setExecutor(SimpleAsyncTaskExecutor executor) {
        this.executor = executor;
    }

    private class GetTankStatsSupplier implements Supplier<Collection<TankStats>> {
        private String id;

        private GetTankStatsSupplier(String id) {
            this.id = id;
        }

        @Override
        public Collection<TankStats> get() {
            PlayerStatsContainer response;
            try {
                response = client.target("wot/tanks/stats/")
                        .queryParam("account_id", id)
                        .queryParam("extra", "random")
                        .queryParam("fields", String.join(",", (CharSequence[]) fields))
                        .get(PlayerStatsContainer.class);
            } catch (WotApiException e) {
                throw new RuntimeException(e);
            }
            return response.getData().entrySet().stream()
                    .flatMap(RemoteTankStatsReader.this::getStreamOfTankStats)
                    .collect(Collectors.toList());
        }
    }
}
