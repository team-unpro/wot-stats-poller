package com.teamunpro.reader;

import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.support.AbstractItemStreamItemReader;

import java.util.List;

public abstract class AbstractRemoteBatchingItemReader<T> extends AbstractItemStreamItemReader<T> {
    protected volatile List<T> results;
    private int current = 0;
    private final Object lock = new Object();

    @Override
    public T read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        synchronized (lock) {

            if (results == null || current >= results.size()) {
                doReadBatch();
                current = 0;
            }

            int next = current++;
            if (next < results.size()) {
                return results.get(next);
            } else {
                return null;
            }

        }
    }

    protected abstract void doReadBatch();
}
