package com.teamunpro.reader;

import com.teamunpro.exception.WotApiException;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.FrontInfo;
import com.teamunpro.wot.dto.ProvinceInfoContainer;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamReader;

import java.util.Iterator;
import java.util.stream.Stream;

public class RemoteClanIdReader implements ItemStreamReader<Integer> {
    private static final String[] fields = new String[] {
            "owner_clan_id", "competitors"
    };

    private WotApiClient client;
    private ItemStreamReader<FrontInfo> frontInfoReader;
    private Iterator<Integer> clanIds;
    @Override
    public Integer read() throws Exception {
        if (clanIds == null) {
            Stream<Integer> clanIdsStream = Stream.empty();
            for (FrontInfo front = frontInfoReader.read(); front != null; front = frontInfoReader.read()) {
                clanIdsStream = Stream.concat(clanIdsStream, getNextClansId(front));
            }
            clanIds = clanIdsStream.filter(c -> c != 0).distinct().iterator();
        }
        if (clanIds.hasNext()) {
            return clanIds.next();
        } else {
            return null;
        }
    }

    private Stream<Integer> getNextClansId(FrontInfo frontInfo) throws WotApiException {
        ProvinceInfoContainer response = client.target("wot/globalmap/provinces/")
                .queryParam("fields", String.join(",", (CharSequence[]) fields))
                .queryParam("front_id", frontInfo.getFrontId())
                .get(ProvinceInfoContainer.class);


        return response.getData().stream()
                .flatMap(p -> Stream.concat(Stream.of(p.getOwnerClanId()), p.getCompetitors().stream()));
    }

    public void setClient(WotApiClient client) {
        this.client = client;
    }

    public void setFrontInfoReader(ItemStreamReader<FrontInfo> frontInfoReader) {
        this.frontInfoReader = frontInfoReader;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        frontInfoReader.open(executionContext);
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
        frontInfoReader.update(executionContext);
    }

    @Override
    public void close() throws ItemStreamException {
        frontInfoReader.close();
    }
}
