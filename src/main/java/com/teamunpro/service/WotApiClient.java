package com.teamunpro.service;

import com.google.common.util.concurrent.RateLimiter;
import com.teamunpro.wot.WotApiRequestBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;

@Service
public class WotApiClient {
    private static final String WOT_API_URI_BASE = "https://api.worldoftanks.com/";
    private final Client client;
    private static final double WOT_API_REQUEST_LIMIT = 15;
    private final RateLimiter rateLimiter = RateLimiter.create(WOT_API_REQUEST_LIMIT);

    @Value("${wot.api_key}")
    private String wotApiKey;

    @Autowired
    public WotApiClient(Client client) {
        this.client = client;
    }

    public WotApiRequestBuilder target(String path) {
        return new WotApiRequestBuilder(client.target(WOT_API_URI_BASE + path), this.rateLimiter)
                .queryParam("application_id", wotApiKey);
    }
}
