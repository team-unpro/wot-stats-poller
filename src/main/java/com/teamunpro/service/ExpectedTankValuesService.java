package com.teamunpro.service;

import com.teamunpro.wot.dto.ExpectedTankValues;
import com.teamunpro.wot.dto.ExpectedTankValuesContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ExpectedTankValuesService {
    private final Map<Integer, ExpectedTankValues> expectedTankValuesMap;

    @Autowired
    public ExpectedTankValuesService(ExpectedTankValuesContainer expectedTankValuesContainer) {
        expectedTankValuesMap = new HashMap<>(expectedTankValuesContainer.getData().size());
        expectedTankValuesContainer.getData().forEach(etv -> expectedTankValuesMap.put(etv.getTankId(), etv));
    }

    public ExpectedTankValues getExpectedTankValues(int tankId) {
        return expectedTankValuesMap.get(tankId);
    }
}
