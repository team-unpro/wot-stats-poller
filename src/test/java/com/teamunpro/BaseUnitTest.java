package com.teamunpro;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseUnitTest {
    protected <T> Client createMockClient(T rv, Class<T> responseType) {
        Client client = mock(Client.class);
        WebTarget webTarget = mock(WebTarget.class);
        Invocation.Builder invocBuilder = mock(Invocation.Builder.class);
        when(invocBuilder.get(responseType)).thenReturn(rv);
        when(webTarget.queryParam(any(), any())).thenReturn(webTarget);
        when(webTarget.request()).thenReturn(invocBuilder);
        when(client.target(any(String.class))).thenReturn(webTarget);
        return client;
    }
}
