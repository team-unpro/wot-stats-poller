package com.teamunpro.reader;

import com.teamunpro.BaseUnitTest;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.FrontInfo;
import com.teamunpro.wot.dto.FrontInfoContainer;
import org.junit.Test;

import javax.ws.rs.client.Client;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class RemoteFrontInfoReaderTest extends BaseUnitTest {
    @Test
    public void readOneTest() throws Exception {
        FrontInfoContainer fic = new FrontInfoContainer();
        fic.setStatus("success");
        FrontInfo fie = new FrontInfo();
        fie.setFrontId("0");
        fic.setData(Collections.singletonList(fie));
        Client client = createMockClient(fic, FrontInfoContainer.class);
        WotApiClient wac = new WotApiClient(client);
        RemoteFrontInfoReader fir = new RemoteFrontInfoReader(wac);
        FrontInfo fi = fir.read();
        assertEquals("0", fi.getFrontId());
        assertNull(fir.read());
    }

    @Test
    public void readMultipleTest() throws Exception {
        FrontInfoContainer fic = new FrontInfoContainer();
        fic.setStatus("success");
        fic.setData(IntStream.of(0, 1).boxed().map(i -> {
            FrontInfo fie = new FrontInfo();
            fie.setFrontId(i.toString());
            return fie;
        }).collect(Collectors.toList()));
        Client client = createMockClient(fic, FrontInfoContainer.class);
        WotApiClient wac = new WotApiClient(client);
        RemoteFrontInfoReader fir = new RemoteFrontInfoReader(wac);
        assertEquals("0", fir.read().getFrontId());
        assertEquals("1", fir.read().getFrontId());
        assertNull(fir.read());
    }
}