package com.teamunpro.processor;

import com.teamunpro.BaseUnitTest;
import com.teamunpro.domain.Clan;
import com.teamunpro.dto.Player;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.ClanDetails;
import com.teamunpro.wot.dto.ClanDetailsContainer;
import com.teamunpro.wot.dto.Member;
import org.junit.Test;

import javax.ws.rs.client.Client;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class ClanItemProcessorTest extends BaseUnitTest {
    @Test
    public void processTest() throws Exception {
        ClanDetailsContainer cdc = new ClanDetailsContainer();
        cdc.setStatus("success");
        HashMap<String, ClanDetails> data = new HashMap<>();
        ClanDetails cd = new ClanDetails();
        Member m = new Member();
        cd.setMembers(Collections.singletonList(m));
        data.put("0", cd);
        cdc.setData(data);
        Client client = createMockClient(cdc, ClanDetailsContainer.class);
        WotApiClient wac = new WotApiClient(client);
        ClanItemProcessor cip = new ClanItemProcessor(wac);
        Clan c = new Clan();
        c.setClanId(0);
        List<Player> players = cip.process(c);
        assertEquals(1, players.size());
        assertEquals(m.getAccountId(), players.get(0).getPlayerId());
    }
}