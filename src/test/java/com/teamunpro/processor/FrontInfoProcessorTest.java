package com.teamunpro.processor;

import com.teamunpro.BaseUnitTest;
import com.teamunpro.domain.Clan;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.FrontInfo;
import com.teamunpro.wot.dto.ProvinceInfo;
import com.teamunpro.wot.dto.ProvinceInfoContainer;
import org.junit.Test;

import javax.ws.rs.client.Client;
import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class FrontInfoProcessorTest extends BaseUnitTest {
    @Test
    public void processTest() throws Exception {
        ProvinceInfoContainer pic = new ProvinceInfoContainer();
        pic.setStatus("success");
        ProvinceInfo pi = new ProvinceInfo();
        pi.setOwnerClanId(0);
        pi.setCompetitors(Collections.emptyList());
        pic.setData(Collections.singletonList(pi));
        Client client = createMockClient(pic, ProvinceInfoContainer.class);
        WotApiClient wac = new WotApiClient(client);
        FrontInfoProcessor fip = new FrontInfoProcessor(wac);
        FrontInfo fi = new FrontInfo();
        fi.setFrontId("0");
        Set<Clan> clans = fip.process(fi);
        assertEquals(1, clans.size());
        assertEquals(0, clans.iterator().next().getClanId());
    }

}