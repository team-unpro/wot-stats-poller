package com.teamunpro.processor;

import com.teamunpro.BaseUnitTest;
import com.teamunpro.domain.Player;
import com.teamunpro.dto.TankStats;
import com.teamunpro.service.ExpectedTankValuesService;
import com.teamunpro.service.WotApiClient;
import com.teamunpro.wot.dto.ExpectedTankValues;
import com.teamunpro.wot.dto.ExpectedTankValuesContainer;
import com.teamunpro.wot.dto.PlayerStatsContainer;
import com.teamunpro.wot.dto.TankStatsContainer;
import org.junit.Test;

import javax.ws.rs.client.Client;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class PlayerItemProcessorTest extends BaseUnitTest {
    @Test
    public void processSkipsNoExpectedTankValueTest() throws Exception {
        ExpectedTankValuesContainer etvc = new ExpectedTankValuesContainer();
        etvc.setData(Collections.emptyList());
        ExpectedTankValuesService etvs = new ExpectedTankValuesService(etvc);
        PlayerStatsContainer psc = new PlayerStatsContainer();
        psc.setStatus("success");
        HashMap<String, List<TankStatsContainer>> data = new HashMap<>();
        TankStatsContainer t = new TankStatsContainer();
        t.setTankId(0);
        com.teamunpro.wot.dto.TankStats random = new com.teamunpro.wot.dto.TankStats();
        random.setBattles(1);
        t.setRandom(random);
        data.put("0", Collections.singletonList(t));
        psc.setData(data);
        Client client = createMockClient(psc, PlayerStatsContainer.class);
        PlayerItemProcessor pip = new PlayerItemProcessor(etvs, new WotApiClient(client));
        Player p = new Player();
        p.setId(0);
        List<TankStats> stats = pip.process(p);
        assertEquals(0, stats.size());
    }

    @Test
    public void processSkipsNoBattlesTestTest() throws Exception {
        ExpectedTankValuesContainer etvc = new ExpectedTankValuesContainer();
        ExpectedTankValues etv = new ExpectedTankValues();
        etv.setTankId(0);
        etvc.setData(Collections.singletonList(etv));
        ExpectedTankValuesService etvs = new ExpectedTankValuesService(etvc);
        PlayerStatsContainer psc = new PlayerStatsContainer();
        psc.setStatus("success");
        HashMap<String, List<TankStatsContainer>> data = new HashMap<>();
        TankStatsContainer t = new TankStatsContainer();
        t.setTankId(0);
        com.teamunpro.wot.dto.TankStats random = new com.teamunpro.wot.dto.TankStats();
        random.setBattles(0);
        t.setRandom(random);
        data.put("0", Collections.singletonList(t));
        psc.setData(data);
        Client client = createMockClient(psc, PlayerStatsContainer.class);
        PlayerItemProcessor pip = new PlayerItemProcessor(etvs, new WotApiClient(client));
        Player p = new Player();
        p.setId(0);
        List<TankStats> stats = pip.process(p);
        assertEquals(0, stats.size());
    }

    @Test
    public void processTest() throws Exception {
        ExpectedTankValuesContainer etvc = new ExpectedTankValuesContainer();
        ExpectedTankValues etv = new ExpectedTankValues();
        etv.setTankId(0);
        etvc.setData(Collections.singletonList(etv));
        ExpectedTankValuesService etvs = new ExpectedTankValuesService(etvc);
        PlayerStatsContainer psc = new PlayerStatsContainer();
        psc.setStatus("success");
        HashMap<String, List<TankStatsContainer>> data = new HashMap<>();
        TankStatsContainer t = new TankStatsContainer();
        t.setTankId(0);
        com.teamunpro.wot.dto.TankStats random = new com.teamunpro.wot.dto.TankStats();
        random.setBattles(1);
        t.setRandom(random);
        data.put("0", Collections.singletonList(t));
        psc.setData(data);
        Client client = createMockClient(psc, PlayerStatsContainer.class);
        PlayerItemProcessor pip = new PlayerItemProcessor(etvs, new WotApiClient(client));
        Player p = new Player();
        p.setId(0);
        List<TankStats> stats = pip.process(p);
        assertEquals(1, stats.size());
        assertEquals(0, stats.get(0).getPlayerId());
        assertEquals(0, stats.get(0).getTankId());
    }

}