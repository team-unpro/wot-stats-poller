# wot-stats-poller

[![build status](https://gitlab.com/team-unpro/wot-stats-poller/badges/master/build.svg)](https://gitlab.com/team-unpro/wot-stats-poller/commits/master)

A collection of batch jobs to collect tank stats for world of tanks clans active on the global map
